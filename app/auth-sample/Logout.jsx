import React from 'react'
import { useNavigate } from 'react-router-dom'

const Logout = ({ handleLogout }) => {
  const navigate = useNavigate()

  const handleLogoutClick = () => {
    handleLogout()
    navigate('/login')
  }

  return (
    <div>
      <h2>Logout</h2>
      <button onClick={handleLogoutClick}>Logout</button>
    </div>
  )
}

export default Logout
