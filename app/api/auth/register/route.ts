import { NextResponse } from 'next/server'
import axios from 'axios'
// import { hash } from 'bcrypt'

export async function POST(request: Request) {
  try {
    const { name, email, password, confpassword } = await request.json()
    // validation email dan password
    if (password !== confpassword) {
      return NextResponse.json({ message: 'Password and confirm password do not match' }, { status: 400 })
    }

    const response = await axios.post('http://localhost:5000/users', {
      name: name,
      email: email,
      password: password,
      confPassword: confpassword,
    })

    return NextResponse.json({ message: 'Register is success' })
  } catch (error) {
    console.log({ error })
    if (axios.isAxiosError(error) && error.response) {
      // Jika ada respon error dari server
      return NextResponse.json({ message: 'Failed to register', error: error.response.data }, { status: error.response.status })
    }
    return NextResponse.json({ message: 'Internal server error' }, { status: 500 })
  }
  
}
