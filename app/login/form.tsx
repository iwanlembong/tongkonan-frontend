'use client'

import React, { useState, ChangeEvent, FormEvent } from 'react'
import axios from 'axios'
import Link from 'next/link'

interface FormData {
  email: string
  password: string
}

const LoginForm: React.FC = () => {
  const [formData, setFormData] = useState<FormData>({
    email: '',
    password: '',
  })
  const [message, setMessage] = useState<string>('')

  const handleChange = (e: ChangeEvent<HTMLInputElement>) => {
    const { name, value } = e.target
    setFormData((prevFormData) => ({
      ...prevFormData,
      [name]: value,
    }))
  }

  const handleLogin = async (e: FormEvent) => {
    e.preventDefault()
    try {
      const response = await axios.post('/api/auth/login', formData)
      setMessage(response.data.message)
    } catch (error) {
      if (axios.isAxiosError(error) && error.response) {
        setMessage(error.response.data.message)
      } else {
        setMessage('An error occurred')
      }
    }
  }

  return (
    <div className="flex w-full h-screen">
      <div className="w-full flex items-center justify-center lg:w-1/2">
        <div className="bg-white px-10 py-20 rounded-3xl border-2 border-gray-200">
          <h1 className="text-5xl font-semibold">LogOn Session</h1>
          <p className="font-medium text-lg text-gray-500 mt-4">
            Welcome back! Please enter your details.
          </p>
          <div className="mt-8">
            {message && (
              <p className="text-red-500 text-sm h-4 block">{message}</p>
            )}
            <form onSubmit={handleLogin}>
              <div>
                <label className="text-lg font-medium">Email</label>
                <input
                  type="email"
                  placeholder="Enter your email"
                  name="email"
                  value={formData.email}
                  onChange={handleChange}
                  required
                  className="w-full border-2 border-gray-100 rounded-xl p-4 mt-1 bg-transparent"
                />
              </div>
              <div>
                <label className="text-lg font-medium">Password</label>
                <input
                  type="password"
                  placeholder="Enter your password"
                  name="password"
                  value={formData.password}
                  onChange={handleChange}
                  required
                  className="w-full border-2 border-gray-100 rounded-xl p-4 mt-1 bg-transparent"
                />
              </div>
              <div className="mt-8 flex justify-between items-center">
                <div>
                  <input type="checkbox" id="remember" />
                  <label
                    className="ml-2 font-medium text-base"
                    htmlFor="remember"
                  >
                    Remember for 30 days
                  </label>
                </div>
                <button className="font-medium text-base text-violet-500">
                  Forget password
                </button>
              </div>
              <div className="mt-8 flex flex-col gap-y-4">
                <button
                  type="submit"
                  className="active:scale-[.98] active:duration-75 hover:scale-[1.01] ease-in-out transition-all py-3 rounded-xl bg-violet-500 text-white text-lg font-bold"
                >
                  Login
                </button>
              </div>
            </form>
            <div className="mt-8 flex justify-center items-center">
              <p className="font-medium text-base">Do not have an account?</p>
              <Link
                href="/register"
                className="ml-2 font-medium text-base text-violet-500"
              >
                Register
              </Link>
            </div>
          </div>
        </div>
      </div>
      <div className="hidden relative lg:flex h-full w-1/2 items-center justify-center bg-gray-200">
        <div className="w-60 h-60 bg-gradient-to-tr from-violet-500 to-pink-500 rounded-full animate-bounce" />
        <div className="w-full h-1/2 absolute bottom-0 bg-white/10 backdrop-blur-lg" />
      </div>
    </div>
  )
}

export default LoginForm
