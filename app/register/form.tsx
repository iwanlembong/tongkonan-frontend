'use client'

import React, { useState, ChangeEvent, FormEvent } from 'react'
import axios from 'axios'
import Link from 'next/link'

interface FormData {
  name: string
  email: string
  password: string
  confpassword: string
}

const RegisterForm: React.FC = () => {
  const [formData, setFormData] = useState<FormData>({
    name: '',
    email: '',
    password: '',
    confpassword: '',
  })
  const [message, setMessage] = useState<string>('')

  const handleChange = (e: ChangeEvent<HTMLInputElement>) => {
    const { name, value } = e.target
    setFormData((prevFormData) => ({
      ...prevFormData,
      [name]: value,
    }))
  }

  const handleRegister = async (e: FormEvent) => {
    e.preventDefault()
    try {
      const response = await axios.post('/api/auth/register', formData)
      setMessage(response.data.message)
    } catch (error) {
      if (axios.isAxiosError(error) && error.response) {
        setMessage(error.response.data.message)
      } else {
        setMessage('An error occurred')
      }
    }
  }

  return (
    <div className="flex w-full h-screen">
      <div className="w-full flex items-center justify-center lg:w-1/2">
        <div className="bg-white px-10 py-20 rounded-3xl border-2 border-gray-200">
          <h1 className="text-5xl font-semibold">Register Form</h1>
          <div className="mt-8">
            {message && (
              <p className="text-red-500 text-sm h-4 block">{message}</p>
            )}
            <form onSubmit={handleRegister}>
              <div>
                <label className="text-lg font-medium">Name</label>
                <input
                  type="text"
                  placeholder="Enter your name"
                  name="name"
                  value={formData.name}
                  onChange={handleChange}
                  required
                  className="w-full border-2 border-gray-100 rounded-xl p-4 mt-1 bg-transparent"
                />
              </div>
              <div>
                <label className="text-lg font-medium">Email</label>
                <input
                  type="text"
                  placeholder="Enter your email"
                  name="email"
                  value={formData.email}
                  onChange={handleChange}
                  required
                  className="w-full border-2 border-gray-100 rounded-xl p-4 mt-1 bg-transparent"
                />
              </div>
              <div>
                <label className="text-lg font-medium">Password</label>
                <input
                  type="password"
                  placeholder="Enter your password"
                  name="password"
                  value={formData.password}
                  onChange={handleChange}
                  required
                  className="w-full border-2 border-gray-100 rounded-xl p-4 mt-1 bg-transparent"
                />
              </div>
              <div>
                <label className="text-lg font-medium">Confirm Password</label>
                <input
                  type="password"
                  placeholder="Enter your confirm password"
                  name="confpassword"
                  value={formData.confpassword}
                  onChange={handleChange}
                  required
                  className="w-full border-2 border-gray-100 rounded-xl p-4 mt-1 bg-transparent"
                />
              </div>
              <div className="mt-8 flex flex-col gap-y-4">
                <button
                  type="submit"
                  className="active:scale-[.98] active:duration-75 hover:scale-[1.01] ease-in-out transition-all py-3 rounded-xl bg-violet-500 text-white text-lg font-bold"
                >
                  Register
                </button>
              </div>
            </form>
            <div className="mt-8 flex justify-center items-center">
              <p className="font-medium text-base">Already have an account?</p>
              <Link
                href="/login"
                className="ml-2 font-medium text-base text-violet-500"
              >
                Login
              </Link>
            </div>
          </div>
        </div>
      </div>
      <div className="hidden relative lg:flex h-full w-1/2 items-center justify-center bg-gray-200">
        <div className="w-60 h-60 bg-gradient-to-tr from-violet-500 to-pink-500 rounded-full animate-bounce" />
        <div className="w-full h-1/2 absolute bottom-0 bg-white/10 backdrop-blur-lg" />
      </div>
    </div>
  )
}

export default RegisterForm
